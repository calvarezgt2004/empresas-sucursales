import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Branch, Company } from '../administrador/interfaces/company';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private url: String = 'https://sucursales-node-in6bv.herokuapp.com/api';
  private headersVariable = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private _http: HttpClient) { }

  getCompanys(token: string) {
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.get<Company>(this.url + '/empresas', { headers: headersToken }).pipe(
      map(response => response.Empresas)
    )
  }

  getCompanyId(idCompany: string, token: string): Observable<Company> {
    let headersToken = this.headersVariable.set('Authorization', token);
    return this._http.get<Company>(this.url + '/EmpresaId/' + idCompany, { headers: headersToken })
  }

  registerCompany(user: Company, token: string): Observable<Company> {
    let headersToken = this.headersVariable.set('Authorization', token)
    let parametros = JSON.stringify(user);
    return this._http.post<Company>(this.url + '/registrarEmpresa', parametros, { headers: headersToken });
  }

  editCompany(company: Company, token: string): Observable<Company> {
    let headersToken = this.headersVariable.set('Authorization', token)
    let parametros = JSON.stringify(company);
    return this._http.put<Company>(this.url + '/editarEmpresa/' + company._id, parametros, { headers: headersToken })
  }

  deleteCompany(idCompany: string, token: string): Observable<Company> {
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.delete<Company>(this.url + '/eliminarEmpresa/' + idCompany, { headers: headersToken })
  }

  getBranch(idCompany: string, token: string) {
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.get<Branch[]>(this.url + '/SurcursalesAdmin/' + idCompany, { headers: headersToken });
  }

}
