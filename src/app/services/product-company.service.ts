import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductsCompany } from '../usuario/interfaces/productsCompany';

@Injectable({
  providedIn: 'root'
})
export class ProductCompanyService {

  private url: String = 'https://sucursales-node-in6bv.herokuapp.com/api';
  private headersVariable = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private _http: HttpClient) { }

  getProduct(token: string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.get<ProductsCompany>(this.url + '/ProductosEmpresa', { headers: headersToken })
  }

  getProductoId(id: string, token: string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token)

    return this._http.get<ProductsCompany>(this.url + '/ProductoId/' + id, { headers: headersToken })
  }

  addProduct(product: ProductsCompany, token: string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token)
    let params = JSON.stringify(product);

    return this._http.post<ProductsCompany>(this.url + '/AgregarproductosEmpresas', params, { headers: headersToken })
  }

  editProduct(product: ProductsCompany, token: string): Observable<ProductsCompany> {
    let params = JSON.stringify(product);
    let headersToken = this.headersVariable.set('Authorization', token)

    return this._http.put<ProductsCompany>(this.url + '/EditarProductosEmpresas/' + product._id, params, { headers: headersToken })
  }

  deleteProduct(id: String, token: string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token);
    return this._http.delete<ProductsCompany>(this.url + '/eliminarProductoEmpresa/' + id, { headers: headersToken })
  }

  getProductLargestStock(token: string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.get<ProductsCompany>(this.url + '/OrdenarStockMayor', { headers: headersToken })
  }

  getProductSmallestStock(token: string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.get<ProductsCompany>(this.url + '/OrdenarStockMenor', { headers: headersToken })
  }

}
