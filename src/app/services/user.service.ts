import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, switchMap, tap } from 'rxjs';
import { User } from '../auth/interfaces/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private token!: string;
  private identidad!: User;
  public User!: User;

  public url: String = 'https://sucursales-node-in6bv.herokuapp.com/api';
  //public url: String = 'http://localhost:3000/api'
  public headersVariable = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(public _http: HttpClient, private router: Router) { }

  login(user: User): Observable<User> {

    const email = user.email
    const password = user.password

    let params = JSON.stringify(user);

    return this._http.post<User>(this.url + '/login', { email, password }, { headers: this.headersVariable }).pipe(
      tap((res) => {
        localStorage.setItem('identity', JSON.stringify(res.usuario))
      }),
      tap(({usuario}) => {
        this.User = usuario!;
      }), 
      switchMap((_) => this._http.post<User>(this.url + '/login', params, { headers: this.headersVariable }))
    )
  }

  register(user: User):Observable<User> {
    const params = JSON.stringify(user);
    return this._http.post<User>(this.url + '/registrarEmpresa', params, { headers: this.headersVariable}).pipe(
      tap((_) => { this.router.navigate(['/auth/login']);})
    )
  }

  editInformation(user: User, token: string): Observable<User>{
    let params = JSON.stringify(user);
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.put<User>(this.url + '/editarEmpresa/'+ user._id, params, { headers: headersToken}).pipe(
      tap((res) => {
        localStorage.removeItem('identity')
        localStorage.setItem('identity', JSON.stringify(res.usuario))
      } )
    )
  }

  getToken() {
    var token2 = localStorage.getItem("token");
    if (token2 != undefined) {
      this.token = token2
    } else {
      this.token = '';
    }

    return this.token;
  }

  clearToken() {
    this.User = null!;
    localStorage.clear();
  }

  getIdentidad() {
    var identidad2 = JSON.parse(localStorage.getItem('identity')!);
    if (identidad2 != null) {
      this.identidad = identidad2;
    }

    return this.identidad;
  }

}
