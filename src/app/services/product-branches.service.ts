import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ProductsCompany } from '../usuario/interfaces/productsCompany';

@Injectable({
  providedIn: 'root'
})
export class ProductBranchesService {

  public url: String = 'https://sucursales-node-in6bv.herokuapp.com/api';
  public headersVariable = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(public _http: HttpClient) { }

  getProductBranches(idSurcursal:string, token:string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.get<ProductsCompany>(this.url + '/VerProductosPorSucursales/' + idSurcursal, { headers: headersToken })
  }

  getProductId(id:string, token: string): Observable<ProductsCompany> {

    let headersToken = this.headersVariable.set('Authorization', token );
    return this._http.get<ProductsCompany>(this.url + '/ProductosSurcursalesId/'+id, { headers: headersToken})
  }
  
  simulateSale( product: ProductsCompany, token:string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token)
    let parametros = JSON.stringify(product);
    return this._http.put<ProductsCompany>(this.url + '/VentaSimuladaSurcursal/' + product.idSurcursal, parametros, { headers: headersToken })
  }

  getProductStokMajorBranch(id:string, token:string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token )
    return this._http.get<ProductsCompany>(this.url+'/stockMasAlto/'+ id, { headers: headersToken})
  }

  getProductStokMinorBranch(id:string, token:string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token )
    return this._http.get<ProductsCompany>(this.url+'/stockMasBajo/'+ id, { headers: headersToken})
  }

  getProductMoreSaleBranch(id:string, token:string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token )
    return this._http.get<ProductsCompany>(this.url+'/ElProductoMasVendido/'+ id, { headers: headersToken})
  }

}
