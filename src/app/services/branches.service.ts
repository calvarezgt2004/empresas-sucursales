import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Branches } from '../usuario/interfaces/branche';
import { ProductsCompany } from '../usuario/interfaces/productsCompany';

@Injectable({
  providedIn: 'root'
})
export class BranchesService {

  public url: String = 'https://sucursales-node-in6bv.herokuapp.com/api';
  public headersVariable = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(public _http: HttpClient) { }

  getBranches(token: string): Observable<Branches> {
    let headersToken = this.headersVariable.set('Authorization', token);
    return this._http.get<Branches>(this.url + '/Sucursales', { headers: headersToken })
  }

  addBranch(branches: Branches, token: string): Observable<Branches> {
    let headersToken = this.headersVariable.set('Authorization', token)
    let params = JSON.stringify(branches)
    return this._http.post<Branches>(this.url + '/agregarSucursales', params, { headers: headersToken })
  }

  getBrancheId(id: string, token: string): Observable<Branches> {
    let headersToken = this.headersVariable.set('Authorization', token);
    return this._http.get(this.url + '/SucursalesId/' + id, { headers: headersToken })
  }

  editBrach(branch: Branches, token: string): Observable<Branches> {
    let parametros = JSON.stringify(branch);
    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.put(this.url + '/editarSurcursal/' + branch._id, parametros, { headers: headersToken })
  }

  deleteBranch(id: String, token: string): Observable<Branches> {

    let headersToken = this.headersVariable.set('Authorization', token)
    return this._http.delete(this.url + '/eliminarSucursales/' + id, { headers: headersToken })
  }

  sendProduct(product:ProductsCompany , token:string): Observable<ProductsCompany> {
    let headersToken = this.headersVariable.set('Authorization', token )
    let params = JSON.stringify(product);
    return this._http.put<ProductsCompany>(this.url+'/EnviarProductosSurcursales/'+ product._id, params, { headers: headersToken })
  }

}
