import { Pipe, PipeTransform } from '@angular/core';
import { Company } from '../administrador/interfaces/company';

@Pipe({
  name: 'nameCompany'
})
export class NameCompanyPipe implements PipeTransform {

  transform(company: Company[], search: string): Company[] {

    if (search === '') {
      return company;
    } else {
      return company.filter(res => {
        return res.nombre?.toLocaleLowerCase().includes(search.toLocaleLowerCase());
      })
    }
  }

}
