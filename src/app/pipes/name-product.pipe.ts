import { Pipe, PipeTransform } from '@angular/core';
import { ProductsCompany } from '../usuario/interfaces/productsCompany';

@Pipe({
  name: 'nameProduct'
})
export class NameProductPipe implements PipeTransform {

  transform(product: ProductsCompany[], search: string, transform: string, name: string): ProductsCompany[] {

    if (search !== '') {
      return product.filter(res => {
        return res.NombreProducto?.toLocaleLowerCase().includes(search.toLowerCase());
      })
    } else if (transform !== '') {
      return product.filter(res => {
        return res.NombreProveedor?.toLocaleLowerCase().includes(transform.toLowerCase());
      })
    } else if (name !== '') {
      return product.filter(res => {
        return res.NombreProductoSucursal?.toLocaleLowerCase().includes(name.toLowerCase());
      })
    }
    else {
      return product;
    }

  }

}
