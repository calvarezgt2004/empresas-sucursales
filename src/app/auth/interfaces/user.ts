export interface User {
    _id?: string;
    nombre: string;
    email: string;
    telefono: number;
    direccion: string;
    password?: string;
    tipoEmpresa: string;
    rol?: string;
    obtenerToken?:string;
    token?:string;
    usuario?:User
}