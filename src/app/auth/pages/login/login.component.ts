import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { Toast } from '../../components/alert'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private fb: FormBuilder, private _userService: UserService, private router: Router) { }

  loader: boolean = true;

  loginForm: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
    obtenerToken: ['true']
  })

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  ngOnInit(): void {
  }

  login() {
    this.loader = false;
    this._userService.login(this.loginForm.value).subscribe({
      next: (value) => {
        localStorage.setItem('token', JSON.stringify(value.token));
        if (this._userService.getIdentidad().rol === 'ROL_EMPRESA') {
          this.router.navigate(['/user/dashboard']);
        } else if (this._userService.getIdentidad().rol === 'ROL_ADMIN') {
          this.router.navigate(['/admin/dashboard']);
        } else {
          return
        }
      },
      error: (err) => {
        this.loader = true;
        Toast.fire({
          icon: 'error',
          title: err.error.mensaje
        })
        this.loginForm.reset()
      },
      complete: () => {
        Toast.fire({
          icon: 'success',
          title: 'Login in successfully'
        })
        this.loader = true;
        this.loginForm.reset()
      }
    })

  }

}
