import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Repit } from 'src/environments/repit';
import { UserService } from '../../../services/user.service';
import { Toast } from '../../components/alert'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public departamentos = Repit.departamentos
  public tipes = Repit.tipoEmpresas
  newOption: boolean = false;
  loader: boolean = true;

  registerForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    telefono: ['', [Validators.required]],
    direccion: ['', [Validators.required]],
    password: ['', [Validators.required]],
    tipoEmpresa: ['', [Validators.required]]
  })

  get tipoEmpresa() {
    return this.registerForm.get('tipoEmpresa')
  }

  otherOption() {
    if (this.tipoEmpresa?.value === 'other') {
      this.newOption = true;
    } else {
      this.newOption = false;
    }

  }

  constructor(private fb: FormBuilder, private userService: UserService) { }

  ngOnInit(): void {
  }

  register() {
    this.loader = false;
    this.userService.register(this.registerForm.value).subscribe({
      next: () => {
      },
      error: (err) => {
        this.loader = true;
        Toast.fire({
          icon: 'error',
          title: err.error.mensaje
        })
        this.registerForm.reset()
      },
      complete: () => {
        this.loader = true;
        Toast.fire({
          icon: 'success',
          title: 'Acount create successfully'
        })
        this.registerForm.reset()
      },
    })
  }

}
