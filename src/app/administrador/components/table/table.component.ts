import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from 'src/app/services/admin.service';
import { Repit } from 'src/environments/repit';
import { Company } from '../../interfaces/company';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Output() infoCompany: EventEmitter<Company> = new EventEmitter();
  @Input() search: string = '';
  @Input() dataTable!: Company[];
  @Input() title!: string;
  @Output() idCompany: EventEmitter<string> = new EventEmitter();
  public addres = Repit.departamentos;
  public selectCompany = Repit.tipoEmpresas;
  newOption: boolean = false;
  private token$ = localStorage.getItem('token') || '';
  public branchs:Company[] = [];
  

  editForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    telefono: ['', [Validators.required]],
    direccion: ['', [Validators.required]],
    password: ['', [Validators.required]],
    tipoEmpresa: ['', [Validators.required]],
    _id: ['', [Validators.required]]
  })

  constructor(private adminSevice: AdminService, private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  get tipoEmpresa() {
    return this.editForm.get('tipoEmpresa')
  }

  otherOption() {
    if (this.tipoEmpresa?.value === 'other') {
      this.newOption = true;
    } else {
      this.newOption = false;
    }

  }

  getInfo(id: string) {
    this.adminSevice.getCompanyId(id, this.token$).subscribe({
      next: ({ Empresa }) => {
        this.editForm.reset({
          nombre: Empresa.nombre,
          email: Empresa.email,
          telefono: Empresa.telefono,
          direccion: Empresa.direccion,
          tipoEmpresa: Empresa.tipoEmpresa,
          _id: Empresa._id
        })
      }
    })
  }

  delete(id: string){
    this.idCompany.emit(id)
  }

  editCompany() {
    this.infoCompany.emit(this.editForm.value);
  }

  getBranches(id: string) {
    this.adminSevice.getBranch(id, this.token$).subscribe({
      next:(value: any) =>{
        this.branchs = value.Sucursales
      },
    })
  }

}
