import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Toast } from 'src/app/auth/components/alert';
import { AdminService } from 'src/app/services/admin.service';
import { Repit } from 'src/environments/repit';
import { Company } from '../../interfaces/company';
import Swal from 'sweetalert2'
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private token: string = localStorage.getItem('token') || '';
  public title: string = 'See companies'
  public addres = Repit.departamentos;
  public selectCompany = Repit.tipoEmpresas;
  public company: Company[] = [];
  newOption: boolean = false;
  search: string = '';
  loader: boolean = false;

  registerForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    telefono: ['', [Validators.required]],
    direccion: ['', [Validators.required]],
    password: ['', [Validators.required]],
    tipoEmpresa: ['', [Validators.required]]
  })

  constructor(private adminService: AdminService,
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this.getCompany();
  }

  get tipoEmpresa() {
    return this.registerForm.get('tipoEmpresa')
  }

  otherOption() {
    if (this.tipoEmpresa?.value === 'other') {
      this.newOption = true;
    } else {
      this.newOption = false;
    }

  }

  getCompany() {
    this.loader = true;
    this.adminService.getCompanys(this.token).subscribe({
      next: (value) => {
        this.company = value;
      },
      error: (err) => {
        this.loader = false;
      },
      complete: () => {
        this.loader = false;
      },
    })
  }

  postCompany() {
    this.adminService.registerCompany(this.registerForm.value, this.token).subscribe({
      next: (value) => {
        Toast.fire({
          icon: 'success',
          title: 'Acount create successfully'
        })
        this.getCompany();
      },
      error: (err) => {
        Toast.fire({
          icon: 'error',
          title: err.error.mensaje
        })
      },
    })
  }

  editCompany(company: Company) {
    this.adminService.editCompany(company, this.token).subscribe({
      next: (value) => {
        Toast.fire({
          icon: 'success',
          title: 'Company edit successfully'
        })
        this.getCompany();
      },
      error: (err) => {
        Toast.fire({
          icon: 'error',
          title: err.error.mensaje
        })
      }
    })

  }

  deleteCompany(id: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.adminService.deleteCompany(id, this.token).subscribe({
          next: (value) => {
            Toast.fire({
              icon: 'success',
              title: 'Eliminate successfully'
            })
            this.getCompany();
          },
          error: (err) => {
            Toast.fire({
              icon: 'error',
              title: err.error.mensaje
            })
          }
        })

      }
    })



  }

  logOut() {
    this.userService.clearToken()
    this.router.navigate(['']);
    Toast.fire({
      icon: 'success',
      title: 'Log out successfully'
    })
  }

}
