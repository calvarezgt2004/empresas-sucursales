import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministradorRoutingModule } from './administrador-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { TableComponent } from './components/table/table.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NameCompanyPipe } from '../pipes/name-company.pipe';
import { LoaderComponent } from './components/loader/loader.component';


@NgModule({
  declarations: [
    DashboardComponent,
    TableComponent,
    NameCompanyPipe,
    LoaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AdministradorRoutingModule,
    ReactiveFormsModule
  ]
})
export class AdministradorModule { }
