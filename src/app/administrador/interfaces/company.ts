export interface Company {
    _id: string;
    nombre: string;
    email: string;
    telefono: string;
    direccion: string;
    tipoEmpresa: string;
    Empresas: Company[];
    Empresa: Company;
}

export interface Branch{
    nombre: string;
    direccion: string;
    telefono: string;
    Sucursales: Branch[];
}