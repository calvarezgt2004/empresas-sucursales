import { ActivatedRoute } from '@angular/router';
import { ChartData, ChartType } from 'chart.js';
import { Component, OnInit } from '@angular/core';
import { ProductBranchesService } from '../../../services/product-branches.service';
import { ProductsCompany } from '../../interfaces/productsCompany';
import { Toast } from 'src/app/auth/components/alert';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-products-branches',
  templateUrl: './products-branches.component.html',
  styleUrls: ['./products-branches.component.scss']
})
export class ProductsBranchesComponent implements OnInit {

  public products$: ProductsCompany[] = [];
  private id$!: string;
  private token$!: string;
  name$: string = 'Product Sale';
  nameChart: string[] = []
  valueChart: number[] = []
  loader: boolean = false;

  constructor(private activatedRoute: ActivatedRoute,
    private productService: ProductBranchesService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(({ id }) => this.id$ = id);
    this.token$ = this.userService.getToken();
    this.getProduct();
  }

  getProduct() {
    this.loader = true
    this.productService.getProductBranches(this.id$, this.token$).subscribe({
      next: (value) => {
        this.products$ = value.Productos;
        this.products$.forEach(a => {
          this.nameChart.push(a.NombreProductoSucursal!)
          this.valueChart.push(a.CantidadVendida!)
        })
        this.doughnutChartData = {
          labels: this.nameChart,
          datasets: [{
            data: this.valueChart
          }]
        }
      },
      error: (err) => {
        this.loader = false
      },
      complete: () => {
        this.loader = false
      },
    });
  }

  sellProduct(product: ProductsCompany) {
    this.productService.simulateSale(product, this.token$).subscribe({
      next: (value) => {

        Toast.fire({
          icon: 'success',
          title: 'Sell product successfully'
        })
        this.getProduct();
      },
      error: (err) => {
        console.log(err);
        Toast.fire({
          icon: 'error',
          title: err.error.Surcusal

        })
      },
    })
  }

  /*Grafica*/

  public doughnutChartLabels: string[] = [
  ];

  public doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: []
  };
  public doughnutChartType: ChartType = 'doughnut';


}
