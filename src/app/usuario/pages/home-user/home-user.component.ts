import { Component, OnInit } from '@angular/core';
import { Toast } from 'src/app/auth/components/alert';
import { BranchesService } from 'src/app/services/branches.service';
import { UserService } from 'src/app/services/user.service';
import { User } from '../../../auth/interfaces/user';
import { Branches } from '../../interfaces/branche';
import Swal from 'sweetalert2'
import { ProductsCompany } from '../../interfaces/productsCompany';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.scss']
})
export class HomeUserComponent implements OnInit {

  user$!: User;
  token$!: string;
  branches$: Branches[] = [];
  loader: boolean = false;

  constructor(private userService: UserService, private branchesService: BranchesService) { }

  ngOnInit(): void {
    this.user$ = this.userService.getIdentidad();
    this.token$ = this.userService.getToken();
    this.getBranches();
  }

  getBranches() {
    this.loader = true;
    this.branchesService.getBranches(this.token$).subscribe({
      next: (response: Branches) => {
        this.branches$ = response.Sucursales!;
      },
      error: (err) => {
        this.loader = false
      },
      complete: () => {
        this.loader = false;
      },
    })
  }

  editInformation(information: User) {
    this.userService.editInformation(information, this.token$).subscribe({
      next: (value) => {
        Toast.fire({
          icon: 'success',
          title: 'Edit successfully'
        })
        this.user$ = this.userService.getIdentidad();
      },
      error: (err) => {
        Toast.fire({
          icon: 'error',
          title: err.error.mensaje
        })
      },
    })
  }

  addBranch(information: Branches) {
    this.branchesService.addBranch(information, this.token$).subscribe({
      next: (value) => {
        Toast.fire({
          icon: 'success',
          title: 'Add branch successfully'
        })
        this.getBranches()
      },
      error: (err) => {
        Toast.fire({
          icon: 'error',
          title: 'Error'
        })
      },
    })
  }

  editBranch(branch: Branches) {
    this.branchesService.editBrach(branch, this.token$).subscribe({
      next: (value) => {
        Toast.fire({
          icon: 'success',
          title: `Edit branch successfully`
        })
        this.getBranches()
      },
      error: (err) => {
        Toast.fire({
          icon: 'error',
          title: 'Error'
        })
      },
    })
  }

  delete(id: String) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.branchesService.deleteBranch(id, this.token$).subscribe({
          next: (value) => {
            Toast.fire({
              icon: 'success',
              title: 'Eliminate successfully'
            })
            this.getBranches();
          },
          error: (err) => {
            console.log(err);
          }
        })
      }
    })
  }

  sendProduct(product: ProductsCompany) {
    this.branchesService.sendProduct(product, this.token$).subscribe({
      next: (value) => {
        Toast.fire({
          icon: 'success',
          title: `Product send successfully`
        })
        this.getBranches()
      },
      error: (err) => {
        Toast.fire({
          icon: 'error',
          title: err.error.Surcusal
        })
        console.log(err);
      },
    })
  }

}
