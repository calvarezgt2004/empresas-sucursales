import { Component, OnInit } from '@angular/core';
import { ProductCompanyService } from 'src/app/services/product-company.service';
import { UserService } from 'src/app/services/user.service';
import { Toast } from 'src/app/auth/components/alert';
import { ProductsCompany } from '../../interfaces/productsCompany';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  private token$!: string;
  public products$: ProductsCompany[] = [];
  name$: string = 'Provider';
  loader: boolean = false;

  constructor(private userService: UserService, private productService: ProductCompanyService) { }

  ngOnInit(): void {
    this.token$ = this.userService.getToken();
    this.getProducts()
  }

  getProducts() {
    this.loader = true
    this.productService.getProduct(this.token$).subscribe({
      next: (value) => {
        this.products$ = value.Productos;
      },
      error: (err) => {
        this.loader = false
      },
      complete: () => {
        this.loader = false
      },
    })
  }

  addProduct(product: ProductsCompany) {
    this.productService.addProduct(product, this.token$).subscribe({
      next: (value) => {
        Toast.fire({
          icon: 'success',
          title: 'Add product successfully'
        })
        this.getProducts()
      },
      error: (err) => {
        Toast.fire({
          icon: 'error',
          title: err.error.mensaje,
        })
      }
    })
  }

  edit(product: ProductsCompany) {
    this.productService.editProduct(product, this.token$).subscribe({
      next: (value) => {
        Toast.fire({
          icon: 'success',
          title: 'edit product successfully'
        })
        this.getProducts()
      },
      error: (err) => {
        Toast.fire({
          icon: 'error',
          title: err.error.mensaje,
        })
      },
    })
  }

  deleteProduct(id: String) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.productService.deleteProduct(id, this.token$).subscribe({
          next: (value) => {
            Toast.fire({
              icon: 'success',
              title: 'Eliminate successfully'
            })
            this.getProducts()
          },
          error: (err) => {
            Toast.fire({
              icon: 'error',
              title: err.error.mensaje,
            })
          }
        })
      }
    })



  }

}
