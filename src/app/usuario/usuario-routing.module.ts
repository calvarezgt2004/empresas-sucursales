import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductsBranchesComponent } from './pages/products-branches/products-branches.component';
import { HomeUserComponent } from './pages/home-user/home-user.component';
import { AuthGuard } from '../guard/auth.guard';

const routes: Routes = [
  {
    path: '', 
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children:[
      {
        path: 'dashboard',
        component: HomeUserComponent
      },
      { 
        path: 'products',
        component: ProductsComponent
      },
      {
        path: 'products/:id',
        component: ProductsBranchesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
