import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BranchesService } from 'src/app/services/branches.service';
import { ProductCompanyService } from 'src/app/services/product-company.service';
import { Repit } from 'src/environments/repit';
import { Branches } from '../../interfaces/branche';
import { ProductsCompany } from '../../interfaces/productsCompany';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.scss']
})
export class CardProductComponent implements OnInit {

  @Input() branches: Branches[] = [];
  @Output() brachDelete: EventEmitter<String> = new EventEmitter();
  @Output() sendProducts: EventEmitter<ProductsCompany> = new EventEmitter();
  @Output() branchInformation: EventEmitter<Branches> = new EventEmitter();
  @Output() editBrach: EventEmitter<Branches> = new EventEmitter();
  private token: string = localStorage.getItem('token') || '';
  branchInfo!: Branches;
  clean!: Branches;
  products: string[] = []
  departamentos = Repit.departamentos;

  branchesForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    telefono: ['', [Validators.required]],
    direccion: ['', [Validators.required]],
    _id: ['', [Validators.required]]
  })

  sendProductsForm: FormGroup = this.fb.group({
    NombreProductoSucursal: ['', [Validators.required]],
    StockSurcursal: ['', [Validators.required]],
    _id: ['']
  })

  constructor(private fb: FormBuilder, private branchesService: BranchesService, private productCompany: ProductCompanyService) { }

  ngOnInit(): void {
    this.getProductsCompany()
  }

  addBranch() {
    this.branchInformation.emit(this.branchesForm.value)
    this.branchesForm.reset();
  }

  getInfo(id: string): void {
    this.branchesService.getBrancheId(id, this.token).subscribe(({ Sucursal }) => {
      this.branchInfo = Sucursal!;
      this.branchesForm.reset({
        nombre: Sucursal?.nombre,
        telefono: Sucursal?.telefono,
        direccion: Sucursal?.direccion,
        _id: Sucursal?._id
      })
    })
  }

  edit() {
    this.editBrach.emit(this.branchesForm.value);
    this.branchInfo = this.clean;
    this.branchesForm.reset();
  }

  delete(id: string) {
    this.branchesService.getBrancheId(id, this.token).subscribe({
      next: ({ Sucursal }) => {
        this.brachDelete.emit(Sucursal?._id)
      },
      error: (err) => {
        console.log(err);
      },
    })
  }

  getBranchId(id: string) {
    this.branchesService.getBrancheId(id, this.token).subscribe(({ Sucursal }) => {
      this.branchInfo = Sucursal!;
      this.sendProductsForm.reset({
        _id: this.branchInfo._id
      })
    })
  }

  getProductsCompany() {
    this.productCompany.getProduct(this.token).subscribe({
      next: (value) => {
        value.Productos.forEach((product) => {
          this.products.push(product.NombreProducto);
        })
      }
    })
  }

  close(){
    this.branchInfo = this.clean
  }

  sendProduct(){
    this.sendProducts.emit(this.sendProductsForm.value)
    this.branchInfo = this.clean;
    this.sendProductsForm.reset();
  }

}
