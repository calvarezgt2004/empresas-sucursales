import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductsCompany } from '../../interfaces/productsCompany';
import { ProductCompanyService } from '../../../services/product-company.service';
import { ActivatedRoute } from '@angular/router';
import { ProductBranchesService } from 'src/app/services/product-branches.service';

@Component({
  selector: 'app-table-products',
  templateUrl: './table-products.component.html',
  styleUrls: ['./table-products.component.scss']
})
export class TableProductsComponent implements OnInit {

  @Output() addProduct: EventEmitter<ProductsCompany> = new EventEmitter();
  @Output() editProducts: EventEmitter<ProductsCompany> = new EventEmitter();
  @Output() deleteProduct: EventEmitter<string> = new EventEmitter();
  @Output() sellProduct: EventEmitter<ProductsCompany> = new EventEmitter();
  @Input() products: ProductsCompany[] = [];
  @Input() name: string = ''
  public id$!: string;
  was: string = '';
  wes: string = '';
  wus: string = '';
  productInfo!: ProductsCompany;
  clean!: ProductsCompany;
  token: string = localStorage.getItem('token') || '';

  addProductForm: FormGroup = this.fb.group({
    NombreProducto: ['', [Validators.required]],
    NombreProveedor: ['', [Validators.required]],
    Stock: ['', [Validators.required]],
    _id: ['', [Validators.required]]
  })

  sellProductForm: FormGroup = this.fb.group({
    NombreProductoSucursal: ['', [Validators.required]],
    StockSurcursal: ['', [Validators.required]],
    idSurcursal: ['', [Validators.required]]
  })

  constructor(private fb: FormBuilder,
    private productService: ProductCompanyService,
    private productServiceBranch: ProductBranchesService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(({ id }) => this.id$ = id);
  }

  add() {
    this.addProduct.emit(this.addProductForm.value)
  }

  getInfo(id: string) {
    this.productService.getProductoId(id, this.token).subscribe((res: any) => {
      this.productInfo = res.Productos
      this.addProductForm.reset({
        NombreProducto: res.Productos.NombreProducto,
        NombreProveedor: res.Productos.NombreProveedor,
        Stock: res.Productos.Stock,
        _id: res.Productos._id
      })
    })
  }

  edit() {
    this.editProducts.emit(this.addProductForm.value)
    this.productInfo = this.clean;
    this.addProductForm.reset();
  }

  delete(id: string) {
    this.deleteProduct.emit(id.trim())
  }

  orderMax() {
    if (this.id$) {
      this.productServiceBranch.getProductStokMajorBranch(this.id$, this.token).subscribe({
        next: (value) => {
          this.products = value.Productos;
        },
      })
    } else {
      this.productService.getProductLargestStock(this.token).subscribe({
        next: (value) => {
          this.products = value.Productos;
        },
      })
    }

  }

  orderMin() {
    if (this.id$) {
      this.productServiceBranch.getProductStokMinorBranch(this.id$, this.token).subscribe({
        next: (value) => {
          this.products = value.Productos;
        }
      })
    } else {
      this.productService.getProductSmallestStock(this.token).subscribe({
        next: (value) => {
          this.products = value.Productos;
        },
      })
    }

  }

  orderBestSellingProduct() {
    this.productServiceBranch.getProductMoreSaleBranch(this.id$, this.token).subscribe({
      next: (value) => {
        this.products = value.Productos;
      }
    })
  }

  getInfoProductBranch(id: string) {
    this.productServiceBranch.getProductId(id, this.token).subscribe({
      next: (value: any) => {
        this.sellProductForm.reset({
          NombreProductoSucursal: value.Productos.NombreProductoSucursal,
          idSurcursal: this.id$
        })
      },
      error: (err) => {
      }
    })
  }

  sell() {
    this.sellProduct.emit(this.sellProductForm.value);
    this.sellProductForm.reset();
  }

}
