import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/auth/interfaces/user';
import { UserService } from 'src/app/services/user.service';
import { Repit } from 'src/environments/repit';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() user!: User;
  @Output() onEdit: EventEmitter<User> = new EventEmitter();

  public departamentos = Repit.departamentos
  public tipes = Repit.tipoEmpresas
  newOption: boolean = false;

  editForm: FormGroup = this.fb.group({
    nombre: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    direccion: ['', [Validators.required]],
    telefono: ['', [Validators.required]],
    tipoEmpresa: ['', [Validators.required]]
  })

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.editForm.reset({
      nombre: this.user.nombre,
      email: this.user.email,
      direccion: this.user.direccion,
      telefono: this.user.telefono,
      tipoEmpresa: this.user.tipoEmpresa
    })
  }

  get tipoEmpresa() {
    return this.editForm.get('tipoEmpresa')
  }

  otherOption() {
    if (this.tipoEmpresa?.value === 'other') {
      this.newOption = true;
    } else {
      this.newOption = false;
    }

  }

  edit() {
    this.onEdit.emit(this.editForm.value)
  }

}
