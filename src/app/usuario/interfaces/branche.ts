export interface Branches {
    _id?: string;
    nombre?: string;
    telefono?: string
    direccion?: string;
    idEmpresa?: string
    Sucursal?: Branches;
    Sucursales?: Branches[];
}