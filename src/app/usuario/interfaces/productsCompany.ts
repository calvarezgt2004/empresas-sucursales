export interface ProductsCompany{
    _id?: string;
    NombreProductoSucursal? : string;
    CantidadVendida?: number;
    StockSurcursal? : number;
    idSurcursal?: string;
    NombreProducto: string;
    NombreProveedor: string;
    Stock: string;
    idEmpresa: string;
    Productos: ProductsCompany[];
}