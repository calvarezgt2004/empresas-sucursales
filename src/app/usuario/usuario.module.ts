import { CardComponent } from './components/card/card.component';
import { CardProductComponent } from './components/card-branches/card-product.component';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NameProductPipe } from '../pipes/name-product.pipe';
import { NgChartsModule } from 'ng2-charts';
import { NgModule } from '@angular/core';
import { ProductsBranchesComponent } from './pages/products-branches/products-branches.component';
import { ProductsComponent } from './pages/products/products.component';
import { RouterModule } from '@angular/router';
import { TableProductsComponent } from './components/table-products/table-products.component';
import { UsuarioRoutingModule } from './usuario-routing.module';
import { NavbarUserComponent } from './components/navbar-user/navbar-user.component';
import { HomeUserComponent } from './pages/home-user/home-user.component';
import { LoaderComponent } from './components/loader/loader.component';


@NgModule({
  declarations: [
    CardComponent,
    CardProductComponent,
    DashboardComponent,
    NameProductPipe,
    ProductsBranchesComponent,
    ProductsComponent,
    TableProductsComponent,
    NavbarUserComponent,
    HomeUserComponent,
    LoaderComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgChartsModule,
    ReactiveFormsModule,
    RouterModule,
    UsuarioRoutingModule
  ],
  
})
export class UsuarioModule { }
